﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class Program : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPublic { get; set; }
        
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Workout> Workouts { get; set; }
        public virtual ICollection<ProgramRating> Ratings { get; set; }
    }
}
