﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class Muscle : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ExerciseMuscle> Exercises { get; set; }
    }
}
