﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class User : IdentityUser<int>
    {
        public virtual Settings Settings { get; set; }

        public virtual ICollection<Program> Programs { get; set; }
        public virtual ICollection<Session> Sessions { get; set; }
        public virtual ICollection<ProgramRating> Ratings { get; set; }
    }
}
