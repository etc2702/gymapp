﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public enum Difficulty
    {
        Low,
        Medium,
        Hard
    }

    public enum Equipment
    {
        None,
        Barbell,
        Dumbbell,
        EzBar,
        Kettlebell,
        Machine,
        Band,
        Other
    }

    public class Exercise : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Difficulty Difficulty { get; set; }
        public Equipment Equipment { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<ExerciseMuscle> Muscles { get; set; }
    }
    
    public class ExerciseMuscle
    {
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public int MuscleId { get; set; }
        public virtual Muscle Muscle { get; set; }
    }
}
