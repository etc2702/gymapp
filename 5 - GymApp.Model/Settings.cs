﻿using GymApp.Model.Base;

namespace GymApp.Model
{
    public enum Units
    {
        Kilograms,
        Pounds
    }

    public class Settings : Entity
    {
        public Units Units { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
