﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class Session : Entity
    {
        public string Name { get; set; }

        public int? WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }
        
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<SessionBlock> SessionBlocks { get; set; }
    }
}
