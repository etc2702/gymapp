﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class SessionBlock : Entity
    {
        public int SessionId { get; set; }
        public virtual Session Session { get; set; }

        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public virtual ICollection<SessionSet> SessionSets { get; set; }
    }
}
