﻿using GymApp.Model.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymApp.Model
{
    public class SessionSet : Entity
    {
        [Column(TypeName = "decimal(5, 2)")]
        public decimal Weight { get; set; }
        public int Reps { get; set; }
        public int Rest { get; set; }
        
        public int SessionBlockId { get; set; }
        public virtual SessionBlock SessionBlock { get; set; }
    }
}
