﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class Block : Entity
    {
        public int WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }

        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public virtual ICollection<Set> Sets { get; set; }
    }
}
