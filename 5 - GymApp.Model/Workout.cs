﻿using GymApp.Model.Base;
using System.Collections.Generic;

namespace GymApp.Model
{
    public class Workout : Entity
    {
        public string Name { get; set; }

        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }

        public virtual ICollection<Block> Blocks { get; set; }
    }
}
