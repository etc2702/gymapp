﻿using GymApp.Data;
using GymApp.Model.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GymApp.Repository.Base
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        private readonly GymAppContext _context;
        private readonly DbSet<T> _entities;

        public Repository(GymAppContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _entities.ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await _entities.FirstOrDefaultAsync(s => s.Id == id);
        }
        
        public async Task Create(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            await _entities.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var existingEntity = await _context.FindAsync<T>(entity.Id);

            if (existingEntity == null)
            {
                throw new ArgumentNullException();
            }
            
            _context.Entry(existingEntity).CurrentValues.SetValues(entity);

            await _context.SaveChangesAsync();
        }

        public void SetChildrenForUpdate(object item, bool isChild = false)
        {
            var props = item.GetType().GetProperties();
            foreach (var prop in props)
            {
                object value = prop.GetValue(item);
                if (prop.PropertyType.IsInterface && value != null)
                {
                    foreach (object iItem in (System.Collections.IEnumerable)value)
                    {
                        SetChildrenForUpdate(iItem, true);
                    }
                }
            }

            var id = (int)item.GetType().GetProperty("Id").GetValue(item);
            if (id == default(int))
            {
                _context.Entry(item).State = EntityState.Added;
            }
            else
            {
                if (isChild)
                {
                    _context.Entry(item).State = EntityState.Modified;
                }
            }
        }

        public async Task Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            _entities.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<T> Clone(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            var existingEntity = await _context.FindAsync<T>(entity.Id);

            if (existingEntity == null)
            {
                throw new ArgumentNullException();
            }

            return (T)_context.Entry(existingEntity).CurrentValues.ToObject();
        }
    }
}
