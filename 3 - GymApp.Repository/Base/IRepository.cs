﻿using GymApp.Model.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GymApp.Repository.Base
{
    public interface IRepository<T> where T : Entity
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task Create(T entity);
        void SetChildrenForUpdate(object item, bool isChild = false);
        Task Update(T entity);
        Task Delete(T entity);
        Task<T> Clone(T entity);
    }
}
