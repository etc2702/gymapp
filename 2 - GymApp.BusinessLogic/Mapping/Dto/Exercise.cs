﻿using GymApp.Model;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Exercise : Base.Dto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Difficulty Difficulty { get; set; }
        public Equipment Equipment { get; set; }

        public int? UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<ExerciseMuscle> Muscles { get; set; }
    }
    
    public class ExerciseMuscle
    {
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public int MuscleId { get; set; }
        public virtual Muscle Muscle { get; set; }
    }
}
