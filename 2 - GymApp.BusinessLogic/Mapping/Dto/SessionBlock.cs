﻿using System.Collections.Generic;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class SessionBlock : Base.Dto
    {
        public int SessionId { get; set; }
        public virtual Session Session { get; set; }

        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public virtual ICollection<SessionSet> SessionSets { get; set; }
    }
}
