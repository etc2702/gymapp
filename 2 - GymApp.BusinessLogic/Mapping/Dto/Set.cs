﻿using System.ComponentModel.DataAnnotations.Schema;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Set : Base.Dto
    {
        [Column(TypeName = "decimal(5, 2)")]
        public decimal Weight { get; set; }
        public int Reps { get; set; }
        public int Rest { get; set; }

        public int BlockId { get; set; }
        public virtual Block Block { get; set; }
    }
}
