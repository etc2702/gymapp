﻿using System.Collections.Generic;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Session : Base.Dto
    {
        public string Name { get; set; }

        public int? WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
        
        public virtual ICollection<SessionBlock> SessionBlocks { get; set; }
    }

    public class Max
    {
        public string ExerciseName { get; set; }
        public decimal MaxWeight { get; set; }
    }
}
