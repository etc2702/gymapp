﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Program : Base.Dto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsPublic { get; set; }

        public decimal AverageRating
        {
            get
            {
                if (Ratings != null && Ratings.Any())
                {
                    return (decimal)Ratings.Average(r => r.Rating);
                }
                else
                {
                    return Decimal.Zero;
                }
            }
        }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Workout> Workouts { get; set; }
        public virtual ICollection<ProgramRating> Ratings { get; set; }
    }
}
