﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace GymApp.BusinessLogic.Mapping.Dto.Base
{
    public class Dto
    {
        [JsonProperty(Order = -2)]
        public int Id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? UpdatedAt { get; set; }
    }
}
