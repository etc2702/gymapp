﻿using GymApp.Model;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class User : IdentityUser<int>
    {
        public int SettingsId { get; set; }
        public virtual Settings Settings { get; set; }

        //public virtual ICollection<Program> Programs { get; set; }
        //public virtual ICollection<Session> Sessions { get; set; }
        //public virtual ICollection<ProgramRating> Ratings { get; set; }
    }

    public class UserRegister
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class UserLogin
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }

    public class UserForgotPassword
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class UserResetPassword
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class UserPrograms
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }
    }

    public class UserSettings : Base.Dto
    {
        public Units Units { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
