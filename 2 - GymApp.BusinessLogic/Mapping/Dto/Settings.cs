﻿using GymApp.Model;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Settings : Base.Dto
    {
        public Units Units { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
