﻿using System.Collections.Generic;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Workout : Base.Dto
    {
        public string Name { get; set; }

        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }

        public virtual ICollection<Block> Blocks { get; set; }
    }
}
