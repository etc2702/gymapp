﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class ProgramRating
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime GivenAt { get; set; }

        public int ProgramId { get; set; }
        public virtual Program Program { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int Rating { get; set; }
    }
}
