﻿using System.Collections.Generic;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class Muscle : Base.Dto
    {
        public string Name { get; set; }
        public string Description { get; set; }

        //public virtual ICollection<ExerciseMuscle> Exercises { get; set; }
    }
}
