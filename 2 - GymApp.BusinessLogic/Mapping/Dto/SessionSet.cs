﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace GymApp.BusinessLogic.Mapping.Dto
{
    public class SessionSet : Base.Dto
    {
        [Column(TypeName = "decimal(5, 2)")]
        public decimal Weight { get; set; }
        public int Reps { get; set; }
        public int Rest { get; set; }
        
        public int SessionBlockId { get; set; }
        public virtual SessionBlock SessionBlock { get; set; }
    }
}
