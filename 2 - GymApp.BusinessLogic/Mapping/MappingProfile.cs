﻿using AutoMapper;
using GymApp.Model;

namespace GymApp.BusinessLogic.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, Dto.User>().ReverseMap();
            CreateMap<Settings, Dto.Settings>().ReverseMap();
            CreateMap<Muscle, Dto.Muscle>().ReverseMap();
            CreateMap<Exercise, Dto.Exercise>().ReverseMap();
            CreateMap<ExerciseMuscle, Dto.ExerciseMuscle>().ReverseMap();
            CreateMap<Program, Dto.Program>().ForMember(x => x.AverageRating, opt => opt.Ignore()).ReverseMap();
            CreateMap<ProgramRating, Dto.ProgramRating>().ReverseMap();
            CreateMap<Workout, Dto.Workout>().ReverseMap();
            CreateMap<Block, Dto.Block>().ReverseMap();
            CreateMap<Set, Dto.Set>().ReverseMap();
            
            CreateMap<Session, Dto.Session>().ReverseMap();
            CreateMap<SessionBlock, Dto.SessionBlock>().ReverseMap();
            CreateMap<SessionSet, Dto.SessionSet>().ReverseMap();
        }
    }
}
