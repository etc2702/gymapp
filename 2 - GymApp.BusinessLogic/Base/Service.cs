﻿using AutoMapper;
using GymApp.BusinessLogic.Mapping.Dto.Base;
using GymApp.Model.Base;
using GymApp.Repository.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GymApp.BusinessLogic.Base
{
    public class Service<TEntity, TDto> : IService<TEntity, TDto> 
        where TEntity : Entity 
        where TDto : Dto
    {
        protected readonly IMapper _mapper;
        protected readonly IRepository<TEntity> _repository;

        public Service(IMapper mapper, IRepository<TEntity> repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<IEnumerable<TDto>> GetAll()
        {
            return _mapper.Map<IEnumerable<TDto>>(await _repository.GetAll());
        }

        public async Task<TDto> GetById(int id)
        {
            return _mapper.Map<TDto>(await _repository.GetById(id));
        }
        
        public async Task Create(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);
            await _repository.Create(entity);
            dto.Id = entity.Id;
        }

        public async Task Update(int id, TDto dto)
        {
            var entity = await _repository.GetById(id);
            entity = _mapper.Map<TEntity>(dto);
            await _repository.Update(entity);
        }

        public async Task Remove(int id)
        {
            var entity = await _repository.GetById(id);
            await _repository.Delete(entity);
        }
    }
}
