﻿using GymApp.BusinessLogic.Mapping.Dto.Base;
using GymApp.Model.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GymApp.BusinessLogic.Base
{
    public interface IService<TEntity, TDto> 
        where TEntity : Entity 
        where TDto : Dto
    {
        Task<IEnumerable<TDto>> GetAll();
        Task<TDto> GetById(int id);
        Task Create(TDto entity);
        Task Update(int id, TDto entity);
        Task Remove(int id);
    }
}
