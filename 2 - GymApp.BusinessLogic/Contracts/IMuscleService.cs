﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.BusinessLogic.Contracts
{
    public interface IMuscleService : IService<Muscle, Dto.Muscle> { }
}
