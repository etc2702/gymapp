﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.BusinessLogic.Contracts
{
    public interface ISetService : IService<Set, Dto.Set, Dto.Set, Dto.Set> { }
}
