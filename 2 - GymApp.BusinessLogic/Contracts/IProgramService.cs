﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GymApp.BusinessLogic.Contracts
{
    public interface IProgramService : IService<Program, Dto.Program>
    {
        Task<Dto.ProgramRating> GetRating(int userId, int programId);
        Task Rate(Dto.ProgramRating rating);
        Task<IEnumerable<Dto.Program>> GetAllPublic();
        Task<IEnumerable<Dto.Program>> GetAllByUserId(int userId);
        Task CopyProgram(int userId, int programId);
    }
}
