﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.BusinessLogic.Contracts
{
    public interface IBlockService : IService<Block, Dto.Block> { }
}
