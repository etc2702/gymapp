﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Contracts
{
    public interface ISessionService : IService<Session, Dto.Session>
    {
        Task<IEnumerable<Dto.Session>> GetAllByUserId(int userId);
        Dto.Session GetWorkoutTemplate(Dto.Workout workout);
        Task<IEnumerable<Dto.Max>> GetMaxes(int userId);
    }
}
