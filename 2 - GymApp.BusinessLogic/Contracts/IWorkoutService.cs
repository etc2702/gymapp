﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Contracts
{
    public interface IWorkoutService : IService<Workout, Dto.Workout>
    {
        Task<IEnumerable<Dto.Block>> GetBlocks(int workoutId);
        Task<Dto.Block> GetBlockById(int workoutId, int blockId);
        Task AddBlock(int workoutId, Dto.Block block);
        Task UpdateBlock(int workoutId, int blockId, Dto.Block block);
        Task RemoveBlock(int workoutId, int blockId);

        Task<IEnumerable<Dto.Set>> GetSets(int workoutId, int blockId);
        Task<Dto.Set> GetSetById(int workoutId, int blockId, int setId);
        Task AddSet(int workoutId, int blockId, Dto.Set set);
        Task UpdateSet(int workoutId, int blockId, int setId, Dto.Set set);
        Task RemoveSet(int workoutId, int blockId, int setId);
    }
}
