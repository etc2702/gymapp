﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Contracts
{
    public interface IExerciseService : IService<Exercise, Dto.Exercise>
    {
        Task AddMuscle(int exerciseId, int muscleId);
        Task RemoveMuscle(int exerciseId, int muscleId);
        Task<IEnumerable<Dto.Exercise>> GetAllByUserId(int userId);
    }
}
