﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace GymApp.BusinessLogic.Implementations
{
    public class SessionService : Service<Session, Dto.Session>, ISessionService
    {
        public SessionService(IMapper mapper, IRepository<Session> repository) : base(mapper, repository) { }

        public async Task<IEnumerable<Dto.Session>> GetAllByUserId(int userId)
        {
            var sessions = await _repository.GetAll();
            return _mapper.Map<IEnumerable<Dto.Session>>(sessions.Where(s => s.UserId == userId).OrderByDescending(s => s.CreatedAt));
        }

        public async Task<IEnumerable<Dto.Max>> GetMaxes(int userId)
        {
            var sessions = await _repository.GetAll();

            var squatLabel = "Squat";
            var benchLabel = "Bench Press";
            var deadliftLabel = "Deadlift";
            var pressLabel = "Overhead Press";

            var maxSquat = new Dto.Max { ExerciseName = squatLabel, MaxWeight = 0m };
            var maxBench = new Dto.Max { ExerciseName = benchLabel, MaxWeight = 0m };
            var maxDeadlift = new Dto.Max { ExerciseName = deadliftLabel, MaxWeight = 0m };
            var maxPress = new Dto.Max { ExerciseName = pressLabel, MaxWeight = 0m };

            foreach (var session in sessions.Where(s => s.UserId == userId))
            {
                if (session.SessionBlocks.Any(b => b.Exercise.Name == squatLabel))
                {
                    var maxSquatSession = session.SessionBlocks.Where(b => b.Exercise.Name == squatLabel).Select(b => b.SessionSets.Any() ? b.SessionSets.Max(s => s.Weight) : 0m).Max();
                    if (maxSquatSession > maxSquat.MaxWeight) maxSquat.MaxWeight = maxSquatSession;
                }
                if (session.SessionBlocks.Any(b => b.Exercise.Name == benchLabel))
                {
                    var maxBenchSession = session.SessionBlocks.Where(b => b.Exercise.Name == benchLabel).Select(b => b.SessionSets.Any() ? b.SessionSets.Max(s => s.Weight) : 0m).Max();
                    if (maxBenchSession > maxBench.MaxWeight) maxBench.MaxWeight = maxBenchSession;
                }
                if (session.SessionBlocks.Any(b => b.Exercise.Name == deadliftLabel))
                {
                    var maxDeadliftSession = session.SessionBlocks.Where(b => b.Exercise.Name == deadliftLabel).Select(b => b.SessionSets.Any() ? b.SessionSets.Max(s => s.Weight) : 0m).Max();
                    if (maxDeadliftSession > maxDeadlift.MaxWeight) maxDeadlift.MaxWeight = maxDeadliftSession;
                }
                if (session.SessionBlocks.Any(b => b.Exercise.Name == pressLabel))
                {
                    var maxPressSession = session.SessionBlocks.Where(b => b.Exercise.Name == pressLabel).Select(b => b.SessionSets.Any() ? b.SessionSets.Max(s => s.Weight) : 0m).Max();
                    if (maxPressSession > maxPress.MaxWeight) maxPress.MaxWeight = maxPressSession;
                }
            }

            return new List<Dto.Max>{ maxSquat, maxBench, maxDeadlift, maxPress };
        }

        public Dto.Session GetWorkoutTemplate(Dto.Workout workout)
        {
            var session = new Dto.Session { Name = workout.Name, WorkoutId = workout.Id, Workout = workout };

            if (session.SessionBlocks == null)
            {
                session.SessionBlocks = new List<Dto.SessionBlock>();
            }

            foreach (var block in workout.Blocks)
            {
                var sessionBlock = new Dto.SessionBlock
                {
                    ExerciseId = block.ExerciseId,
                    Exercise = block.Exercise
                };

                session.SessionBlocks.Add(sessionBlock);

                if (sessionBlock.SessionSets == null)
                {
                    sessionBlock.SessionSets = new List<Dto.SessionSet>();
                }

                foreach (var set in block.Sets)
                {
                    sessionBlock.SessionSets.Add(new Dto.SessionSet
                    {
                        Reps = set.Reps,
                        Weight = set.Weight,
                        Rest = set.Rest
                    });
                }
            }

            return session;
        }
    }
}
