﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Implementations
{
    public class ProgramService : Service<Program, Dto.Program>, IProgramService
    {
        public ProgramService(IMapper mapper, IRepository<Program> repository) : base(mapper, repository) { }

        public async Task<Dto.ProgramRating> GetRating(int userId, int programId)
        {
            var program = await _repository.GetById(programId);
            var rating = program.Ratings.FirstOrDefault(r => r.UserId == userId);
            return _mapper.Map<Dto.ProgramRating>(rating);
        }

        public async Task Rate(Dto.ProgramRating rating)
        {
            var program = await _repository.GetById(rating.ProgramId);
            if (program == null) throw new ArgumentNullException();
            if (program.Ratings.Any(r => r.UserId == rating.UserId))
            {
                var existingRating = program.Ratings.FirstOrDefault(r => r.UserId == rating.UserId);
                existingRating.Rating = rating.Rating;
                existingRating.GivenAt = DateTime.UtcNow;
            }
            else
            {
                program.Ratings.Add(
                    new ProgramRating {
                        ProgramId = rating.ProgramId,
                        UserId = rating.UserId,
                        GivenAt = DateTime.UtcNow,
                        Rating = rating.Rating
                    });
            }

            await _repository.Update(program);
        }
        
        public async Task<IEnumerable<Dto.Program>> GetAllPublic()
        {
            var programs = await _repository.GetAll();
            return _mapper.Map<IEnumerable<Dto.Program>>(programs.Where(p => p.IsPublic == true));
        }

        public async Task<IEnumerable<Dto.Program>> GetAllByUserId(int userId)
        {
            var programs = await _repository.GetAll();
            return _mapper.Map<IEnumerable<Dto.Program>>(programs.Where(p => p.UserId == userId));
        }
        
        public new async Task Update(int id, Dto.Program dto)
        {
            var program = await _repository.GetById(id);
            program = _mapper.Map<Program>(dto);
            _repository.SetChildrenForUpdate(program);
            await _repository.Update(program);
        }

        public async Task CopyProgram(int userId, int programId)
        {
            var program = await _repository.GetById(programId);
            var clone = _mapper.Map<Dto.Program>(program);
            
            clone.Id = default(Int32);
            clone.User = null;
            clone.UserId = userId;
            clone.Ratings = null;
            clone.IsPublic = default(Boolean); ;
            foreach (var workout in clone.Workouts)
            {
                workout.Id = default(Int32);
                foreach (var block in workout.Blocks)
                {
                    block.Id = default(Int32);
                    foreach (var set in block.Sets)
                    {
                        set.Id = default(Int32);
                    }
                    
                    if (block.Exercise.UserId != null) // if it's a custom exercise, user will get a copy too
                    {
                        block.Exercise.Id = default(Int32);
                    }
                }
            }

            await _repository.Create(_mapper.Map<Program>(clone));
        }
    }
}
