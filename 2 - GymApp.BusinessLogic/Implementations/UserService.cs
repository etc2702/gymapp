﻿using AutoMapper;
using GymApp.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.BusinessLogic.Implementations
{
    public class UserService : UserManager<User>
    {
        private readonly IMapper _mapper;

        public UserService(
            IMapper mapper, 
            IUserStore<User> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services, 
            ILogger<UserManager<User>> logger) 
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _mapper = mapper;
        }

        public async Task<Dto.User> GetById(int id)
        {
            var user = await Users.FirstOrDefaultAsync(u => u.Id == id);
            return _mapper.Map<Dto.User>(user);
        }

        public override async Task<IdentityResult> CreateAsync(User user, string password)
        {
            user.Settings = new Settings { Units = Units.Kilograms };
            return await base.CreateAsync(user, password);
        }

        public async Task<Dto.Settings> GetSettingsByUserId(int userId)
        {
            var user = await Users.FirstOrDefaultAsync(u => u.Id == userId);
            return _mapper.Map<Dto.Settings>(user.Settings);
        }

        public async Task UpdateSettings(int userId, Dto.Settings settings, bool isDefault = false)
        {
            if (isDefault)
            {
                settings = new Dto.Settings { Units = Units.Kilograms };
            }
            var user = await Users.FirstOrDefaultAsync(u => u.Id == userId);
            user.Settings.Units = settings.Units;
            await base.UpdateAsync(user);
        }
    }
}
