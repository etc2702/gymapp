﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;

namespace GymApp.BusinessLogic.Implementations
{
    public class SessionBlockService : Service<SessionBlock, Dto.SessionBlock, Dto.SessionBlock, Dto.SessionBlock>, ISessionBlockService
    {
        public SessionBlockService(IMapper mapper, IRepository<SessionBlock> repository) : base(mapper, repository) { }
    }
}
