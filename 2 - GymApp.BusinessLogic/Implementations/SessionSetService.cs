﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;

namespace GymApp.BusinessLogic.Implementations
{
    public class SessionSetService : Service<SessionSet, Dto.SessionSet, Dto.SessionSet, Dto.SessionSet>, ISessionSetService
    {
        public SessionSetService(IMapper mapper, IRepository<SessionSet> repository) : base(mapper, repository) { }
    }
}
