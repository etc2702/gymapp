﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;

namespace GymApp.BusinessLogic.Implementations
{
    public class BlockService : Service<Block, Dto.Block>, IBlockService
    {
        public BlockService(IMapper mapper, IRepository<Block> repository) : base(mapper, repository) { }
    }
}
