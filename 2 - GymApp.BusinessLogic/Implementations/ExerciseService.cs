﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Implementations
{
    public class ExerciseService : Service<Exercise, Dto.Exercise>, IExerciseService
    {
        public ExerciseService(IMapper mapper, IRepository<Exercise> repository) : base(mapper, repository) { }
        
        public async Task AddMuscle(int exerciseId, int muscleId)
        {
            var exercise = await _repository.GetById(exerciseId);
            if (exercise == null) throw new ArgumentNullException();
            if (exercise.Muscles.Any(m => m.MuscleId == muscleId)) throw new ArgumentException();

            exercise.Muscles.Add(new ExerciseMuscle { ExerciseId = exerciseId, MuscleId = muscleId });
            await _repository.Update(exercise);
        }

        public async Task RemoveMuscle(int exerciseId, int muscleId)
        {
            var exercise = await _repository.GetById(exerciseId);
            if (exercise == null) throw new ArgumentNullException();
            var exerciseMuscle = exercise.Muscles.FirstOrDefault(m => m.MuscleId == muscleId);
            if (exerciseMuscle == null) throw new ArgumentNullException();

            exercise.Muscles.Remove(exerciseMuscle);
            await _repository.Update(exercise);
        }

        public async Task<IEnumerable<Dto.Exercise>> GetAllByUserId(int userId)
        {
            var exercises = await _repository.GetAll();
            return _mapper.Map<IEnumerable<Dto.Exercise>>(exercises.Where(s => s.UserId == userId || s.UserId == null));
        }

        public new async Task Update(int id, Dto.Exercise dto)
        {
            var exercise = await _repository.GetById(id);
            exercise = _mapper.Map<Exercise>(dto);
            await _repository.Update(exercise);

            exercise = await _repository.GetById(id);
            foreach (var muscle in exercise.Muscles.ToList())
            {
                await RemoveMuscle(id, muscle.MuscleId);
            }

            foreach (var muscle in dto.Muscles.ToList())
            {
                await AddMuscle(id, muscle.MuscleId);
            }
        }
    }
}
