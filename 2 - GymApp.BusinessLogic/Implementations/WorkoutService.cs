﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Collections.Generic;

namespace GymApp.BusinessLogic.Implementations
{
    public class WorkoutService : Service<Workout, Dto.Workout>, IWorkoutService
    {
        public WorkoutService(IMapper mapper, IRepository<Workout> repository) : base(mapper, repository) { }

        public async Task<IEnumerable<Dto.Block>> GetBlocks(int workoutId)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();

            return _mapper.Map<IEnumerable<Dto.Block>>(workout.Blocks);
        }

        public async Task<Dto.Block> GetBlockById(int workoutId, int blockId)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();

            return _mapper.Map<Dto.Block>(block);
        }

        public async Task AddBlock(int workoutId, Dto.Block block)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();

            var blockModel = _mapper.Map<Block>(block);
            workout.Blocks.Add(blockModel);
            await _repository.Update(workout);
            block.Id = blockModel.Id;
        }

        public async Task UpdateBlock(int workoutId, int blockId, Dto.Block block)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var existingBlock = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();

            _mapper.Map(block, existingBlock);
            await _repository.Update(workout);
        }

        public async Task RemoveBlock(int workoutId, int blockId)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();

            workout.Blocks.Remove(block);
            await _repository.Update(workout);
        }

        public async Task<IEnumerable<Dto.Set>> GetSets(int workoutId, int blockId)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();

            return _mapper.Map<IEnumerable<Dto.Set>>(block.Sets);
        }

        public async Task<Dto.Set> GetSetById(int workoutId, int blockId, int setId)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();
            var set = block.Sets.FirstOrDefault(s => s.Id == setId) ?? throw new ArgumentNullException();

            return _mapper.Map<Dto.Set>(set);
        }

        public async Task AddSet(int workoutId, int blockId, Dto.Set set)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();

            var setModel = _mapper.Map<Set>(set);
            block.Sets.Add(setModel);
            await _repository.Update(workout);
            set.Id = setModel.Id;
        }

        public async Task UpdateSet(int workoutId, int blockId, int setId, Dto.Set set)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();
            var existingSet = block.Sets.FirstOrDefault(s => s.Id == setId) ?? throw new ArgumentNullException();

            _mapper.Map(set, existingSet);
            await _repository.Update(workout);
        }

        public async Task RemoveSet(int workoutId, int blockId, int setId)
        {
            var workout = await _repository.GetById(workoutId) ?? throw new ArgumentNullException();
            var block = workout.Blocks.FirstOrDefault(b => b.Id == blockId) ?? throw new ArgumentNullException();
            var set = block.Sets.FirstOrDefault(s => s.Id == setId) ?? throw new ArgumentNullException();

            block.Sets.Remove(set);
            await _repository.Update(workout);
        }
    }
}
