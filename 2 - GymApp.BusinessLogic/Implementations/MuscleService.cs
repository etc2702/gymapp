﻿using GymApp.Model;
using GymApp.BusinessLogic.Base;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using GymApp.BusinessLogic.Contracts;
using GymApp.Repository.Base;
using AutoMapper;

namespace GymApp.BusinessLogic.Implementations
{
    public class MuscleService : Service<Muscle, Dto.Muscle>, IMuscleService
    {
        public MuscleService(IMapper mapper, IRepository<Muscle> repository) : base(mapper, repository) { }
    }
}
