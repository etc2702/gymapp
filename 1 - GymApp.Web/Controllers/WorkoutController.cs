﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using GymApp.BusinessLogic.Contracts;
using GymApp.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.Web.Controllers
{
    [Route("api/workouts")]
    [ApiController]
    public class WorkoutController : ControllerBase
    {
        private readonly IWorkoutService _workoutService;

        public WorkoutController(IWorkoutService workoutService)
        {
            _workoutService = workoutService;
        }

        // GET: api/workouts
        /// <summary>
        /// Returns all workouts.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/workouts
        ///
        /// </remarks>
        /// <returns>All workouts</returns>
        /// <response code="200">Returns all workouts</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _workoutService.GetAll());
        }

        // GET: api/workouts/1
        /// <summary>
        /// Returns a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/workouts/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A workout</returns>
        /// <response code="200">Returns the workout</response>
        /// <response code="404">Workout could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{id}", Name = "GetWorkout")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([Required] int id)
        {
            var workout = await _workoutService.GetById(id);

            if (workout == null)
            {
                return NotFound();
            }

            return Ok(workout);
        }

        // POST: api/workouts
        /// <summary>
        /// Creates a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /workouts
        ///     {
        ///         "ProgramId": 0,
	    ///         "Name": "string",
	    ///         "Blocks": [
	    ///             {
		///                 "ExerciseId": 0,
		///                 "Sets": [
		///	                    {
		///		                    "Weight": 0,
		///		                    "Reps": 0,
		///		                    "Rest": 0
        ///                     }
		///                 ]
	    ///             }
	    ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param name="workout"></param>
        /// <returns>A newly created workout</returns>
        /// <response code="201">Returns the newly created workout</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] Dto.Workout workout)
        {
            await _workoutService.Create(workout);

            return CreatedAtRoute(
                routeName: "GetWorkout",
                routeValues: new { id = workout.Id },
                value: await _workoutService.GetById(workout.Id));
        }

        // PUT: api/workouts/1
        /// <summary>
        /// Updates a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /workouts/1
        ///     {
        ///         "ProgramId": 0,
	    ///         "Name": "string"
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="workout"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Workout has been updated</response>
        /// <response code="500">An error has ocurred</response>         
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([Required] int id, [FromBody] Dto.Workout workout)
        {
            await _workoutService.Update(id, workout);

            return NoContent();
        }

        // DELETE: api/workouts/1
        /// <summary>
        /// Deletes a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/workouts/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Workout has been deleted</response>
        /// <response code="500">An error has ocurred</response>       
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete([Required] int id)
        {
            await _workoutService.Remove(id);

            return NoContent();
        }

        // GET: api/workouts/1/blocks
        /// <summary>
        /// Returns all blocks from a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/workouts/1/blocks
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <returns>All blocks from a workout</returns>
        /// <response code="200">Returns all blocks from a workout</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{workoutId}/blocks")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetBlocks([Required] int workoutId)
        {
            return Ok(await _workoutService.GetBlocks(workoutId));
        }

        // GET: api/workouts/1/blocks/1
        /// <summary>
        /// Returns a block.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/workouts/1/blocks/1
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="id"></param>
        /// <returns>A block</returns>
        /// <response code="200">Returns the block</response>
        /// <response code="404">Block could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{workoutId}/blocks/{id}", Name = "GetBlock")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetBlockById([Required] int workoutId, [Required] int id)
        {
            var block = await _workoutService.GetBlockById(workoutId, id);

            if (block == null)
            {
                return NotFound();
            }

            return Ok(block);
        }

        // POST: api/workouts/1/blocks
        /// <summary>
        /// Adds a block to a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /workouts/1/blocks
        ///     {
        ///	        "ExerciseId": 0,
        ///	        "Sets": [
        ///	        	{
        ///	        		"Weight": 0,
        ///	        		"Reps": 0,
        ///	        		"Rest": 0
        ///             }
        ///         ]
        ///     }
        /// 
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="block"></param>
        /// <returns>A newly created block</returns>
        /// <response code="201">Returns the newly created block</response>
        /// <response code="500">An error has ocurred</response>         
        [HttpPost("{workoutId}/blocks")]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> AddBlock([Required] int workoutId, [Required] Dto.Block block)
        {
            await _workoutService.AddBlock(workoutId, block);

            return CreatedAtRoute(
                routeName: "GetBlock",
                routeValues: new { id = block.Id },
                value: await _workoutService.GetBlockById(workoutId, block.Id));
        }

        // PUT: api/workouts/1/blocks/1
        /// <summary>
        /// Updates a block from a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /workouts/1/blocks/1
        ///     {
        ///         "WorkoutId": 0,
        ///         "ExerciseId": 0
        ///     }
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <param name="block"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Block has been updated</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPut("{workoutId}/blocks/{blockId}")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateBlock([Required] int workoutId, [Required] int blockId, [Required] Dto.Block block)
        {
            await _workoutService.UpdateBlock(workoutId, blockId, block);

            return NoContent();
        }

        // DELETE: api/workouts/1/blocks/1
        /// <summary>
        /// Removes a block from a workout.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /workouts/1/blocks/1
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Block has been removed from the workout</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpDelete("{workoutId}/blocks/{blockId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RemoveBlock([Required] int workoutId, [Required] int blockId)
        {
            await _workoutService.RemoveBlock(workoutId, blockId);

            return NoContent();
        }

        // GET: api/workouts/1/blocks/1/sets
        /// <summary>
        /// Returns all sets from a block.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/workouts/1/blocks/1/sets
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <returns>All sets from a block</returns>
        /// <response code="200">Returns all sets from a block</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{workoutId}/blocks/{blockId}/sets")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetSets([Required] int workoutId, [Required] int blockId)
        {
            return Ok(await _workoutService.GetSets(workoutId, blockId));
        }

        // GET: api/workouts/1/blocks/1/sets/1
        /// <summary>
        /// Returns a set.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/workouts/1/blocks/1/sets/1
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <param name="id"></param>
        /// <returns>A set</returns>
        /// <response code="200">Returns the set</response>
        /// <response code="404">Set could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{workoutId}/blocks/{blockId}/sets/{id}", Name = "GetSet")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetSetById([Required] int workoutId, [Required] int blockId, [Required] int id)
        {
            var set = await _workoutService.GetSetById(workoutId, blockId, id);

            if (set == null)
            {
                return NotFound();
            }

            return Ok(set);
        }

        // POST: api/workouts/1/blocks/1/sets
        /// <summary>
        /// Adds a set to a block.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /workouts/1/blocks/1/sets
        ///     {
        ///	        "Weight": 0,
        ///	        "Reps": 0,
        ///	        "Rest": 0
        ///     }
        /// 
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <param name="set"></param>
        /// <returns>A newly created set</returns>
        /// <response code="201">Returns the newly created set</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpPost("{workoutId}/blocks/{blockId}/sets")]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> AddSet([Required] int workoutId, [Required] int blockId, [Required] Dto.Set set)
        {
            await _workoutService.AddSet(workoutId, blockId, set);

            return CreatedAtRoute(
                routeName: "GetSet",
                routeValues: new { id = set.Id },
                value: await _workoutService.GetSetById(workoutId, blockId, set.Id));
        }

        // PUT: api/workouts/1/blocks/1/sets/1
        /// <summary>
        /// Updates a set from a block.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /workouts/1/blocks/1/sets/1
        ///     {
        ///         "BlockId": 0
        ///	        "Weight": 0,
        ///	        "Reps": 0,
        ///	        "Rest": 0
        ///     }
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <param name="setId"></param>
        /// <param name="set"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Set has been updated</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPut("{workoutId}/blocks/{blockId}/sets/{setId}")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateSet([Required] int workoutId, [Required] int blockId, [Required] int setId, [Required] Dto.Set set)
        {
            await _workoutService.UpdateSet(workoutId, blockId, setId, set);

            return NoContent();
        }

        // DELETE: api/workouts/1/blocks/1/set/1
        /// <summary>
        /// Removes a set from a block.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /workouts/1/blocks/1/set/1
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <param name="blockId"></param>
        /// <param name="setId"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Set has been removed from the block</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpDelete("{workoutId}/blocks/{blockId}/sets/{setId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RemoveSet([Required] int workoutId, [Required] int blockId, [Required] int setId)
        {
            await _workoutService.RemoveSet(workoutId, blockId, setId);

            return NoContent();
        }
    }
}
