﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using GymApp.BusinessLogic.Contracts;
using GymApp.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.Web.Controllers
{
    [Route("api/programs")]
    [ApiController]
    public class ProgramController : ControllerBase
    {
        private readonly IProgramService _programService;

        public ProgramController(IProgramService programService)
        {
            _programService = programService;
        }

        // GET: api/programs
        /// <summary>
        /// Returns all programs not marked as private.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/programs
        ///
        /// </remarks>
        /// <returns>All programs not marked as private</returns>
        /// <response code="200">Returns all programs not marked as private</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _programService.GetAllPublic());
        }

        // GET: api/programs/1
        /// <summary>
        /// Returns a program.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/programs/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A program</returns>
        /// <response code="200">Returns the program</response>
        /// <response code="404">Program could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{id}", Name = "GetProgram")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([Required] int id)
        {
            var program = await _programService.GetById(id);
            if (program == null) return NotFound();

            return Ok(program);
        }

        // POST: api/programs
        /// <summary>
        /// Creates a program.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /programs
        ///     {
        ///         "Name": "string",
        ///         "Description": "string",
        ///         "IsPublic": false,
        ///         "Workouts": [
        ///             {
        ///                 "Name": "string",
        ///                 "Blocks": [
        ///                     {
        ///                         "ExerciseId": 0,
        ///                         "Sets": [
        ///                             {
        ///                                 "Reps": 0,
        ///                                 "Rest": "string"
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param name="program"></param>
        /// <returns>A newly created program</returns>
        /// <response code="201">Returns the newly created program</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] Dto.Program program)
        {
            program.UserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            foreach (var workout in program.Workouts)
            {
                foreach (var block in workout.Blocks)
                {
                    block.Exercise = null;
                }
            }
            await _programService.Create(program);

            return CreatedAtRoute(
                routeName: "GetProgram",
                routeValues: new { id = program.Id },
                value: program);
        }

        // PUT: api/programs
        /// <summary>
        /// Updates a program.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /programs
        ///     {
        ///         "Name": "string",
        ///         "Description": "string",
        ///         "IsPublic": false
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="program"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Program has been updated</response>
        /// <response code="500">An error has ocurred</response>         
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([Required] int id, [FromBody] Dto.Program program)
        {
            program.Ratings = null;
            await _programService.Update(id, program);

            return NoContent();
        }

        // DELETE: api/programs/1
        /// <summary>
        /// Deletes a program.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/programs/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Program has been deleted</response>
        /// <response code="500">An error has ocurred</response>       
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete([Required] int id)
        {
            await _programService.Remove(id);

            return NoContent();
        }
    }
}
