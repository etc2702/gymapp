﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using GymApp.Web.Filters;
using GymApp.BusinessLogic.Contracts;
using Dto = GymApp.BusinessLogic.Mapping.Dto;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using GymApp.Model;
using GymApp.BusinessLogic.Implementations;
using System.Linq;

namespace GymApp.Web.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly UserService _userService;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IExerciseService _exerciseService;
        private readonly IProgramService _programService;
        private readonly ISessionService _sessionService;

        public UserController(
            IConfiguration configuration,
            UserManager<User> userService,
            SignInManager<User> signInManager,
            IEmailSender emailSender,
            IExerciseService exerciseService,
            IProgramService programService,
            ISessionService sessionService)
        {
            _configuration = configuration;
            _userService = (UserService)userService;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _exerciseService = exerciseService;
            _programService = programService;
            _sessionService = sessionService;
        }

        // GET: api/users
        /// <summary>
        /// Returns all users.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users
        ///
        /// </remarks>
        /// <returns>All users</returns>
        /// <response code="200">Returns all users</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _userService.Users.ToListAsync());
        }

        // GET: api/users/1
        /// <summary>
        /// Returns a user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A user</returns>
        /// <response code="200">Returns the user</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{id}", Name = "GetById")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetById([Required] int id)
        {
            var user = await _userService.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (user == null) return NotFound();

            return Ok(user);
        }

        // GET: api/users
        /// <summary>
        /// Returns a user's profile.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users
        ///
        /// </remarks>
        /// <returns>A user's profile</returns>
        /// <response code="200">Returns the user's profile</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("profile", Name = "GetProfile")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetProfile()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var user = await _userService.GetById(id);
            if (user == null) return NotFound();
            
            return Ok(user);
        }

        // GET: api/users/settings
        /// <summary>
        /// Returns a user's settings.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/settings
        ///
        /// </remarks>
        /// <returns>A user's settings</returns>
        /// <response code="200">Returns the user's settings</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("settings", Name = "GetSettings")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetSettings()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var settings = await _userService.GetSettingsByUserId(id);

            return Ok(settings);
        }

        // GET: api/users/exercises
        /// <summary>
        /// Returns a user's exercises.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/exercises
        ///
        /// </remarks>
        /// <returns>A user's exercises</returns>
        /// <response code="200">Returns the user's exercises</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("exercises", Name = "GetExercises")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetExercises()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var exercises = await _exerciseService.GetAllByUserId(id);

            return Ok(exercises);
        }

        // GET: api/users/programs
        /// <summary>
        /// Returns a user's programs.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/programs
        ///
        /// </remarks>
        /// <returns>A user's programs</returns>
        /// <response code="200">Returns the user's programs</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("programs", Name = "GetPrograms")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetPrograms()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var programs = await _programService.GetAllByUserId(id);

            return Ok(programs);
        }

        // GET: api/users/sessions
        /// <summary>
        /// Returns a user's sessions.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/sessions
        ///
        /// </remarks>
        /// <returns>A user's sessions</returns>
        /// <response code="200">Returns the user's sessions</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("sessions", Name = "GetSessions")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetSessions()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var sessions = await _sessionService.GetAllByUserId(id);

            return Ok(sessions);
        }

        // GET: api/users/sessions/last/1
        /// <summary>
        /// Returns a user's last sessions.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/sessions/last/1
        ///
        /// </remarks>
        /// <param name="numberOfSessions"></param>
        /// <returns>A user's last sessions</returns>
        /// <response code="200">Returns the user's last sessions</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("sessions/last/{numberOfSessions}", Name = "GetLastSessions")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetLastSessions(int numberOfSessions)
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var sessions = await _sessionService.GetAllByUserId(id);

            return Ok(sessions.Take(numberOfSessions));
        }

        // GET: api/users/maxes
        /// <summary>
        /// Returns a user's maxes.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/maxes
        ///
        /// </remarks>
        /// <returns>A user's maxes</returns>
        /// <response code="200">Returns the user's maxes</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("maxes", Name = "GetMaxes")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetMaxes()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var maxes = await _sessionService.GetMaxes(id);

            return Ok(maxes);
        }

        // PUT: api/users/settings
        /// <summary>
        /// Updates user settings.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /users/settings
        ///     {
        ///         "Units": 0
        ///     }
        ///
        /// </remarks>
        /// <param name="settings"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Settings have been updated</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPut("settings")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateSettings([Required] Dto.Settings settings)
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            await _userService.UpdateSettings(id, settings);

            return NoContent();
        }

        // PUT: api/users/settings/reset
        /// <summary>
        /// Resets user settings.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /users/settings/reset
        ///
        /// </remarks>
        /// <returns>Nothing</returns>
        /// <response code="204">Settings have been reset</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPut("settings/reset")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> ResetSettings()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            await _userService.UpdateSettings(id, null, true);

            return NoContent();
        }

        // POST: api/users/programs/1/copy
        /// <summary>
        /// Makes a copy of a program.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/programs/1/copy
        ///
        /// </remarks>
        /// <param name="programId"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Program is copied</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpPost("programs/{programId}/copy", Name = "CopyProgram")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> CopyProgram([Required] int programId)
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            await _programService.CopyProgram(userId, programId);

            return NoContent();
        }

        // GET: api/users/programs/1/rate
        /// <summary>
        /// Returns a user's program rating.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/users/programs/1/rate
        ///
        /// </remarks>
        /// <returns>A user's program rating</returns>
        /// <response code="200">Returns the user's program rating</response>
        /// <response code="404">User could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("programs/{programId}/rate", Name = "GetProgramRating")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetProgramRating([Required] int programId)
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var rating = await _programService.GetRating(userId, programId);

            return Ok(rating);
        }

        // POST: api/users/programs/1/rate
        /// <summary>
        /// Rates a program.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/users/programs/1/rate
        ///     {
        ///         "Rating": 0
        ///     }
        ///
        /// </remarks>
        /// <param name="programId"></param>
        /// <param name="rating"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Program is rated</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpPost("programs/{programId}/rate", Name = "RateProgram")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RateProgram([Required] int programId, [Required] Dto.ProgramRating rating)
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            rating.UserId = userId;
            rating.ProgramId = programId;
            await _programService.Rate(rating);

            return NoContent();
        }

        // POST: api/users/register
        /// <summary>
        /// Registers a user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /users/register
        ///     {
        ///         "Email": "string",
        ///         "Password": "string",
        ///         "ConfirmPassword": "string"
        ///     }
        ///
        /// </remarks>
        /// <param name="register"></param>
        /// <returns>A newly registered user</returns>
        /// <response code="201">User has been registered</response>
        /// <response code="400">Registration data invalid</response>       
        /// <response code="500">An error has ocurred</response>        
        [AllowAnonymous]
        [HttpPost("register")]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Register(Dto.UserRegister register)
        {
            var user = new User { UserName = register.UserName, Email = register.Email };
            var result = await _userService.CreateAsync(user, register.Password);

            if (!result.Succeeded) return BadRequest();
            
            return CreatedAtRoute(
                routeName: "GetById",
                routeValues: new { id = user.Id },
                value: await _userService.Users.FirstOrDefaultAsync(u => u.Id == user.Id));
        }

        // POST: api/users/login
        /// <summary>
        /// Logs a user in.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /users/login
        ///     {
        ///         "Email": "string",
        ///         "Password": "string",
        ///         "RememberMe": false
        ///     }
        ///
        /// </remarks>
        /// <param name="login"></param>
        /// <returns>A token</returns>
        /// <response code="200">User has been logged in</response>
        /// <response code="500">An error has ocurred</response>   
        [AllowAnonymous]
        [HttpPost("login")]
        [ValidateModel]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Login(Dto.UserLogin login)
        {
            var user = await _userService.FindByEmailAsync(login.Email);
            if (user == null) return BadRequest();

            var result = await _signInManager.CheckPasswordSignInAsync(user, login.Password, false);
            if (!result.Succeeded) return BadRequest();

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim("email", user.Email)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApiAuth:SecretKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _configuration["ApiAuth:Issuer"],
                _configuration["ApiAuth:Audience"],
                claims,
                expires: DateTime.UtcNow.AddDays(30),
                signingCredentials: creds
            );

            return Ok(new { response = new JwtSecurityTokenHandler().WriteToken(token) });
        }

        // POST: api/users/logout
        /// <summary>
        /// Logs a user out.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /users/logout
        ///
        /// </remarks>
        /// <returns>Nothing</returns>
        /// <response code="200">User has been logged out</response>
        /// <response code="500">An error has ocurred</response>
        [HttpPost("logout")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();

            return Ok();
        }
    }
}