﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using GymApp.BusinessLogic.Contracts;
using GymApp.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.Web.Controllers
{
    [Route("api/sessions")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly ISessionService _sessionService;
        private readonly IWorkoutService _workoutService;

        public SessionController(ISessionService sessionService, IWorkoutService workoutService)
        {
            _sessionService = sessionService;
            _workoutService = workoutService;
        }

        // GET: api/sessions
        /// <summary>
        /// Returns all sessions.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/sessions
        ///
        /// </remarks>
        /// <returns>All sessions</returns>
        /// <response code="200">Returns all sessions</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _sessionService.GetAll());
        }

        // GET: api/sessions/1
        /// <summary>
        /// Returns a session.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/sessions/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A session</returns>
        /// <response code="200">Returns the session</response>
        /// <response code="404">Session could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{id}", Name = "GetSession")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([Required] int id)
        {
            var session = await _sessionService.GetById(id);
            if (session == null) return NotFound();

            if (session.Workout == null)
            {
                session.Workout = new Dto.Workout { Name = "Deleted" };
            }

            return Ok(session);
        }

        // GET: api/sessions/1
        /// <summary>
        /// Returns a session based on its workout template.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/sessions/1/do
        ///
        /// </remarks>
        /// <param name="workoutId"></param>
        /// <returns>A session based on its workout template</returns>
        /// <response code="200">Returns the session based on its workout template</response>
        /// <response code="404">Session could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{workoutId}/do", Name = "DoSession")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> DoSession([Required] int workoutId)
        {
            var userId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            var workout = await _workoutService.GetById(workoutId);
            if (workout.Program.UserId != userId) return Unauthorized();

            var session = _sessionService.GetWorkoutTemplate(workout);

            return Ok(session);
        }

        // POST: api/sessions
        /// <summary>
        /// Creates a session.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /sessions
        ///     {
        ///         "Name": "string",
        ///         "Description": "string",
        ///         "IsPublic": false,
        ///         "Workouts": [
        ///             {
        ///                 "Name": "string",
        ///                 "Blocks": [
        ///                     {
        ///                         "ExerciseId": 0,
        ///                         "Sets": [
        ///                             {
        ///                                 "Reps": 0,
        ///                                 "Rest": "string"
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param name="session"></param>
        /// <returns>A newly created session</returns>
        /// <response code="201">Returns the newly created session</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] Dto.Session session)
        {
            session.UserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            session.Workout = null;
            foreach (var block in session.SessionBlocks)
            {
                block.Exercise = null;
            }
            await _sessionService.Create(session);

            return CreatedAtRoute(
                routeName: "GetSession",
                routeValues: new { id = session.Id },
                value: session);
        }

        // PUT: api/sessions
        /// <summary>
        /// Updates a session.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /sessions
        ///     {
        ///         "Name": "string",
        ///         "Description": "string",
        ///         "IsPublic": false
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="session"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Session has been updated</response>
        /// <response code="500">An error has ocurred</response>         
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([Required] int id, [FromBody] Dto.Session session)
        {
            await _sessionService.Update(id, session);

            return NoContent();
        }

        // DELETE: api/sessions/1
        /// <summary>
        /// Deletes a session.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/sessions/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Session has been deleted</response>
        /// <response code="500">An error has ocurred</response>       
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete([Required] int id)
        {
            await _sessionService.Remove(id);

            return NoContent();
        }
    }
}
