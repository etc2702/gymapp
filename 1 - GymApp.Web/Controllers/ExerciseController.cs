﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using GymApp.BusinessLogic.Contracts;
using GymApp.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.Web.Controllers
{
    [Route("api/exercises")]
    [ApiController]
    public class ExerciseController : ControllerBase
    {
        private readonly IExerciseService _exerciseService;

        public ExerciseController(IExerciseService exerciseService)
        {
            _exerciseService = exerciseService;
        }

        // GET: api/exercises
        /// <summary>
        /// Returns all exercises.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/exercises
        ///
        /// </remarks>
        /// <returns>All exercises</returns>
        /// <response code="200">Returns all exercises</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _exerciseService.GetAll());
        }

        // GET: api/exercises/1
        /// <summary>
        /// Returns an exercise.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/exercises/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>An exercise</returns>
        /// <response code="200">Returns the exercise</response>
        /// <response code="404">Exercise could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{id}", Name = "GetExercise")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([Required] int id)
        {
            var exercise = await _exerciseService.GetById(id);

            if (exercise == null)
            {
                return NotFound();
            }

            return Ok(exercise);
        }

        // POST: api/exercises
        /// <summary>
        /// Creates an exercise.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /exercises
        ///     {
        ///         "Name": "string",
        ///         "Description": "string",
        ///         "Difficulty": 0,
        ///         "Equipment": 0,
        ///         "Muscles": [
        ///             {
        ///                 "Muscle": 
        ///                 {
        ///                     "Id": 0
        ///                 }
        ///             }
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param name="exercise"></param>
        /// <returns>A newly created exercise</returns>
        /// <response code="201">Returns the newly created exercise</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] Dto.Exercise exercise)
        {
            exercise.UserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier)?.Value);
            await _exerciseService.Create(exercise);

            return CreatedAtRoute(
                routeName: "GetExercise",
                routeValues: new { id = exercise.Id },
                value: exercise);
        }

        // PUT: api/exercises
        /// <summary>
        /// Updates an exercise.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /exercises/1
        ///     {
        ///         "Name": "string",
        ///         "Description": "string",
        ///         "Difficulty": 0,
        ///         "Equipment": 0
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="exercise"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Exercise has been updated</response>
        /// <response code="500">An error has ocurred</response>         
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([Required] int id, [FromBody] Dto.Exercise exercise)
        {
            await _exerciseService.Update(id, exercise);

            return NoContent();
        }

        // DELETE: api/exercises/1
        /// <summary>
        /// Deletes an exercise.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/exercises/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Exercise has been deleted</response>
        /// <response code="500">An error has ocurred</response>       
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete([Required] int id)
        {
            await _exerciseService.Remove(id);

            return NoContent();
        }

        // POST: api/exercises/1/muscles/1
        /// <summary>
        /// Adds a muscle to an exercise.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /exercises/1/muscles/1
        ///
        /// </remarks>
        /// <param name="exerciseId"></param>
        /// <param name="muscleId"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Muscle has been added to the exercise</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpPost("{exerciseId}/muscles/{muscleId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> AddMuscle([Required] int exerciseId, [Required] int muscleId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _exerciseService.AddMuscle(exerciseId, muscleId);

            return NoContent();
        }

        // DELETE: api/exercises/1/muscles/1
        /// <summary>
        /// Removes a muscle from an exercise.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /exercises/1/muscles/1
        ///
        /// </remarks>
        /// <param name="exerciseId"></param>
        /// <param name="muscleId"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Muscle has been removed from the exercise</response>
        /// <response code="500">An error has ocurred</response>        
        [HttpDelete("{exerciseId}/muscles/{muscleId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> RemoveMuscle([Required] int exerciseId, [Required] int muscleId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _exerciseService.RemoveMuscle(exerciseId, muscleId);

            return NoContent();
        }
    }
}
