﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using GymApp.BusinessLogic.Contracts;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Dto = GymApp.BusinessLogic.Mapping.Dto;

namespace GymApp.Web.Controllers
{
    [Route("api/muscles")]
    [ApiController]
    public class MuscleController : ControllerBase
    {
        private readonly IMuscleService _muscleService;

        public MuscleController(IMuscleService muscleService)
        {
            _muscleService = muscleService;
        }

        // GET: api/muscles
        /// <summary>
        /// Returns all muscles.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/muscles
        ///
        /// </remarks>
        /// <returns>All muscles</returns>
        /// <response code="200">Returns all muscles</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _muscleService.GetAll());
        }

        // GET: api/muscles/1
        /// <summary>
        /// Returns a muscle.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/muscles/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>A muscle</returns>
        /// <response code="200">Returns the muscle</response>
        /// <response code="404">Muscle could not be found</response>
        /// <response code="500">An error has ocurred</response>      
        [HttpGet("{id}", Name = "GetMuscle")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Get([Required] int id)
        {
            var muscle = await _muscleService.GetById(id);

            if (muscle == null)
            {
                return NotFound();
            }
                
            return Ok(muscle);
        }

        // POST: api/muscles
        /// <summary>
        /// Creates a muscle.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/muscles
        ///     {
        ///         "Name": "string"
        ///     }
        ///
        /// </remarks>
        /// <param name="muscle"></param>
        /// <returns>A newly created muscle</returns>
        /// <response code="201">Returns the newly created muscle</response>
        /// <response code="500">An error has ocurred</response>           
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Create([FromBody] Dto.Muscle muscle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            await _muscleService.Create(muscle);

            return CreatedAtRoute(
                routeName: "GetMuscle",
                routeValues: new { id = muscle.Id },
                value: muscle);
        }

        // PUT: api/muscles
        /// <summary>
        /// Updates a muscle.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/muscles
        ///     {
        ///         "Name": "string"
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="muscle"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Muscle has been updated</response>
        /// <response code="500">An error has ocurred</response>           
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Update([Required] int id, [FromBody] Dto.Muscle muscle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _muscleService.Update(id, muscle);

            return NoContent();
        }

        // DELETE: api/muscles/1
        /// <summary>
        /// Deletes a muscle.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     DELETE /api/muscles/1
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Nothing</returns>
        /// <response code="204">Muscle has been deleted</response>
        /// <response code="500">An error has ocurred</response>           
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> Delete([Required] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _muscleService.Remove(id);

            return NoContent();
        }
    }
}
