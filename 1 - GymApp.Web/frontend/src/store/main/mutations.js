import { LocalStorage } from 'quasar'

export const logIn = (state, token) => {
  let base64Url = token.split('.')[1]
  let base64 = base64Url.replace('-', '+').replace('_', '/')
  let parsed = (JSON.parse(window.atob(base64)))
  let current = Date.now().valueOf() / 1000
  if (current > parsed.exp) {
    return
  }
  state.token = token
  state.userId = parseInt(parsed.sub)
  state.email = parsed.email
  LocalStorage.set('token', token)
}

export const logOut = (state) => {
  state.isLoggedIn = false
  state.token = null
  state.userId = null
  state.email = null
  LocalStorage.remove('token')
}

export const setLogIn = (state, bool) => {
  state.isLoggedIn = bool
}
