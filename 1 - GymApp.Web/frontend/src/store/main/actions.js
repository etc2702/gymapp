import { LocalStorage } from 'quasar'

export const checkLogIn = (store) => {
  if (store.state.token == null && LocalStorage.has('token')) {
    store.commit('logIn', LocalStorage.get.item('token'))
  }
}
