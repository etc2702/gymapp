
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      { path: '', name: 'index', component: () => import('pages/index') },
      { path: 'profile', name: 'profile', component: () => import('pages/profile') },
      { path: 'muscles', name: 'muscles', component: () => import('pages/muscles') },
      { path: 'muscle/:id?', name: 'muscle', component: () => import('pages/muscle') },
      { path: 'exercises', name: 'exercises', component: () => import('pages/exercises') },
      { path: 'exercise/:id?', name: 'exercise', component: () => import('pages/exercise') },
      { path: 'programs', name: 'programs', component: () => import('pages/programs') },
      { path: 'program/:id?', name: 'program', component: () => import('pages/program') },
      { path: 'store', name: 'store', component: () => import('pages/store') },
      { path: 'do/:id?', name: 'do', component: () => import('pages/do') },
      { path: 'sessions', name: 'sessions', component: () => import('pages/sessions') },
      { path: 'session/:id?', name: 'session', component: () => import('pages/session') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/landing'),
    children: [
      { path: 'login', name: 'login', component: () => import('pages/login') },
      { path: 'register', name: 'register', component: () => import('pages/register') }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
