import { Bar, mixins } from 'vue-chartjs'

export default {
  extends: Bar,
  mixins: [mixins.reactiveProp],
  props: ['chartData', 'options'],
  mounted () {
    if (this.chartData) {
      this.renderChart(this.chartData, this.options)
    }
  }
}
