﻿using GymApp.Model;
using GymApp.Model.Base;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace GymApp.Data
{
    public class GymAppContext : IdentityDbContext<User, IdentityRole<int>, int>
    {
        public GymAppContext() { }

        public GymAppContext(DbContextOptions<GymAppContext> options) : base(options) { }

        public new DbSet<User> Users { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<Muscle> Muscles { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Block> Blocks { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<ProgramRating> ProgramRatings { get; set; }
        public DbSet<SessionSet> SessionSets { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionBlock> SessionBlocks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<User>(b => { b.ToTable("Users"); });
            modelBuilder.Entity<IdentityUserClaim<int>>(b => { b.ToTable("UserClaims"); });
            modelBuilder.Entity<IdentityUserLogin<int>>(b => { b.ToTable("UserLogins"); });
            modelBuilder.Entity<IdentityUserToken<int>>(b => { b.ToTable("UserTokens"); });
            modelBuilder.Entity<IdentityRole<int>>(b => { b.ToTable("Roles"); });
            modelBuilder.Entity<IdentityRoleClaim<int>>(b => { b.ToTable("RoleClaims"); });
            modelBuilder.Entity<IdentityUserRole<int>>(b => { b.ToTable("UserRoles"); });

            modelBuilder.Entity<ExerciseMuscle>(b => { b.ToTable("ExerciseMuscles"); });
            modelBuilder.Entity<ExerciseMuscle>()
                .HasKey(t => new { t.ExerciseId, t.MuscleId });

            modelBuilder.Entity<Exercise>()
                .HasMany(e => e.Muscles)
                .WithOne(m => m.Exercise)
                .HasForeignKey(e => e.ExerciseId);

            modelBuilder.Entity<Muscle>()
                .HasMany(m => m.Exercises)
                .WithOne(e => e.Muscle)
                .HasForeignKey(m => m.MuscleId);

            modelBuilder.Entity<ProgramRating>()
                .HasKey(t => new { t.ProgramId, t.UserId });

            modelBuilder.Entity<ProgramRating>()
                .HasOne(r => r.Program)
                .WithMany(p => p.Ratings)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ProgramRating>()
                .HasOne(r => r.User)
                .WithMany(u => u.Ratings)
                .OnDelete(DeleteBehavior.Restrict);
            
            // Initial seeding
            modelBuilder.Entity<Muscle>().HasData(
                new Muscle { Id = 1, Name = "Quadriceps", Description = "Extends the knee", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 2, Name = "Hamstrings", Description = "Extends the thigh, flexes the knee, and also rotates the tibia medially, especially when the knee is flexed", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 3, Name = "Glutes", Description = "Major extensor of hip joint, assists in laterally rotating the thigh", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 4, Name = "Calves", Description = "Powerful plantar flexor of ankle", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 5, Name = "Chest", Description = "Adducts and medially rotates humerus", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 6, Name = "Back", Description = "Extends, adducts, and medially rotates humerus", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 7, Name = "Shoulders", Description = "Flexes and medially rotates arm", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 8, Name = "Triceps", Description = "Chief extensor of forearm", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 9, Name = "Biceps", Description = "Supinates forearm and, when it is supine, flexes forearm", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 10, Name = "Forearms", Description = "Flexes forearm", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 11, Name = "Trapezius", Description = "Elevates, retracts and rotates scapula", CreatedAt = DateTime.MinValue },
                new Muscle { Id = 12, Name = "Abs", Description = "Flex the torso and thigh with respect to each other", CreatedAt = DateTime.MinValue }
            );
            
            modelBuilder.Entity<Exercise>().HasData(
                new Exercise { Id = 1, Name = "Squat", Description = "The barbell squat is arguably the king of all exercises, the only challenger being the barbell deadlift.", Equipment = Equipment.Barbell, CreatedAt = DateTime.MinValue },
                new Exercise { Id = 2, Name = "Bench Press", Description = "Like the barbell squat and barbell deadlift, the barbell bench press is a major mass- and strength-building exercise.", Equipment = Equipment.Barbell, CreatedAt = DateTime.MinValue },
                new Exercise { Id = 3, Name = "Deadlift", Description = "The barbell deadlift is arguably the king of all major mass-building exercises, challenged only by the barbell squat.", Equipment = Equipment.Barbell, CreatedAt = DateTime.MinValue },
                new Exercise { Id = 4, Name = "Overhead Press", Description = "There’s a strict variation of the barbell overhead press known as the military press in which the feet are kept close, as if at attention.", Equipment = Equipment.Barbell, CreatedAt = DateTime.MinValue }
            );
            
            modelBuilder.Entity<ExerciseMuscle>().HasData(
                new ExerciseMuscle { ExerciseId = 1, MuscleId = 1 }, 
                new ExerciseMuscle { ExerciseId = 1, MuscleId = 2 }, 
                new ExerciseMuscle { ExerciseId = 1, MuscleId = 3 }, 
                new ExerciseMuscle { ExerciseId = 1, MuscleId = 4 },
                new ExerciseMuscle { ExerciseId = 1, MuscleId = 12 },
                new ExerciseMuscle { ExerciseId = 2, MuscleId = 5 },
                new ExerciseMuscle { ExerciseId = 2, MuscleId = 7 },
                new ExerciseMuscle { ExerciseId = 2, MuscleId = 8 },
                new ExerciseMuscle { ExerciseId = 2, MuscleId = 12 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 1 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 2 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 3 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 6 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 10 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 11 },
                new ExerciseMuscle { ExerciseId = 3, MuscleId = 12 },
                new ExerciseMuscle { ExerciseId = 4, MuscleId = 3 },
                new ExerciseMuscle { ExerciseId = 4, MuscleId = 7 },
                new ExerciseMuscle { ExerciseId = 4, MuscleId = 8 },
                new ExerciseMuscle { ExerciseId = 4, MuscleId = 12 }
            );

            var admin = new User { Id = 1, UserName = "admin", Email = "admin@gymapp.com", NormalizedEmail = "admin@gymapp.com", SecurityStamp = Guid.NewGuid().ToString() };
            admin.PasswordHash = new PasswordHasher<User>().HashPassword(admin, "123456");

            modelBuilder.Entity<User>().HasData(admin);
            
            modelBuilder.Entity<Settings>().HasData(
                new Settings { Id = 1, UserId = 1, Units = Units.Kilograms }
            );
            
            modelBuilder.Entity<Program>().HasData(
                new Program { Id = 1, UserId = 1, Name = "5x5", Description = "A basic program", IsPublic = true, CreatedAt = DateTime.MinValue }
            );

            modelBuilder.Entity<ProgramRating>().HasData(
                new ProgramRating { ProgramId = 1, UserId = 1, Rating = 5 }
            );

            modelBuilder.Entity<Workout>().HasData(
                new Workout { Id = 1, ProgramId = 1, Name = "Workout A", CreatedAt = DateTime.MinValue },
                new Workout { Id = 2, ProgramId = 1, Name = "Workout B", CreatedAt = DateTime.MinValue }
            );

            modelBuilder.Entity<Block>().HasData(
                new Block { Id = 1, WorkoutId = 1, ExerciseId = 1, CreatedAt = DateTime.MinValue },
                new Block { Id = 2, WorkoutId = 1, ExerciseId = 2, CreatedAt = DateTime.MinValue },
                new Block { Id = 3, WorkoutId = 2, ExerciseId = 3, CreatedAt = DateTime.MinValue },
                new Block { Id = 4, WorkoutId = 2, ExerciseId = 4, CreatedAt = DateTime.MinValue }
            );

            modelBuilder.Entity<Set>().HasData(
                new Set { Id = 1, BlockId = 1, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 2, BlockId = 1, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 3, BlockId = 1, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 4, BlockId = 1, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 5, BlockId = 1, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 6, BlockId = 2, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 7, BlockId = 2, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 8, BlockId = 2, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 9, BlockId = 2, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 10, BlockId = 2, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 11, BlockId = 3, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 12, BlockId = 3, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 13, BlockId = 3, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 14, BlockId = 3, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 15, BlockId = 3, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 16, BlockId = 4, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 17, BlockId = 4, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 18, BlockId = 4, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 19, BlockId = 4, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue },
                new Set { Id = 20, BlockId = 4, Reps = 5, Rest = 1, CreatedAt = DateTime.MinValue }
            );
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            foreach (var entity in ChangeTracker.Entries<Entity>())
            {
                if (entity.State == EntityState.Added)
                {
                    entity.Entity.CreatedAt = DateTime.UtcNow;
                }
                else if (entity.State == EntityState.Modified)
                {
                    entity.Entity.UpdatedAt = DateTime.UtcNow;
                }
            }

            return await base.SaveChangesAsync();
        }
    }
}
