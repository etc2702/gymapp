﻿// <auto-generated />
using System;
using GymApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace _4GymApp.Data.Migrations
{
    [DbContext(typeof(GymAppContext))]
    [Migration("20180823165404_New")]
    partial class New
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.0-rtm-30799")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("GymApp.Model.Block", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("ExerciseId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<int>("WorkoutId");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.HasIndex("WorkoutId");

                    b.ToTable("Blocks");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), ExerciseId = 1, WorkoutId = 1 },
                        new { Id = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), ExerciseId = 2, WorkoutId = 1 },
                        new { Id = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), ExerciseId = 3, WorkoutId = 2 },
                        new { Id = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), ExerciseId = 4, WorkoutId = 2 }
                    );
                });

            modelBuilder.Entity("GymApp.Model.Exercise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<int>("Difficulty");

                    b.Property<int>("Equipment");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Exercises");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "The barbell squat is arguably the king of all exercises, the only challenger being the barbell deadlift.", Difficulty = 0, Equipment = 1, Name = "Squat" },
                        new { Id = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Like the barbell squat and barbell deadlift, the barbell bench press is a major mass- and strength-building exercise.", Difficulty = 0, Equipment = 1, Name = "Bench Press" },
                        new { Id = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "The barbell deadlift is arguably the king of all major mass-building exercises, challenged only by the barbell squat.", Difficulty = 0, Equipment = 1, Name = "Deadlift" },
                        new { Id = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "There’s a strict variation of the barbell overhead press known as the military press in which the feet are kept close, as if at attention.", Difficulty = 0, Equipment = 1, Name = "Overhead Press" }
                    );
                });

            modelBuilder.Entity("GymApp.Model.ExerciseMuscle", b =>
                {
                    b.Property<int>("ExerciseId");

                    b.Property<int>("MuscleId");

                    b.HasKey("ExerciseId", "MuscleId");

                    b.HasIndex("MuscleId");

                    b.ToTable("ExerciseMuscles");

                    b.HasData(
                        new { ExerciseId = 1, MuscleId = 1 },
                        new { ExerciseId = 1, MuscleId = 2 },
                        new { ExerciseId = 1, MuscleId = 3 },
                        new { ExerciseId = 1, MuscleId = 4 },
                        new { ExerciseId = 1, MuscleId = 12 },
                        new { ExerciseId = 2, MuscleId = 5 },
                        new { ExerciseId = 2, MuscleId = 7 },
                        new { ExerciseId = 2, MuscleId = 8 },
                        new { ExerciseId = 2, MuscleId = 12 },
                        new { ExerciseId = 3, MuscleId = 1 },
                        new { ExerciseId = 3, MuscleId = 2 },
                        new { ExerciseId = 3, MuscleId = 3 },
                        new { ExerciseId = 3, MuscleId = 6 },
                        new { ExerciseId = 3, MuscleId = 10 },
                        new { ExerciseId = 3, MuscleId = 11 },
                        new { ExerciseId = 3, MuscleId = 12 },
                        new { ExerciseId = 4, MuscleId = 3 },
                        new { ExerciseId = 4, MuscleId = 7 },
                        new { ExerciseId = 4, MuscleId = 8 },
                        new { ExerciseId = 4, MuscleId = 12 }
                    );
                });

            modelBuilder.Entity("GymApp.Model.Muscle", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.ToTable("Muscles");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Extends the knee", Name = "Quadriceps" },
                        new { Id = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Extends the thigh, flexes the knee, and also rotates the tibia medially, especially when the knee is flexed", Name = "Hamstrings" },
                        new { Id = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Major extensor of hip joint, assists in laterally rotating the thigh", Name = "Glutes" },
                        new { Id = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Powerful plantar flexor of ankle", Name = "Calves" },
                        new { Id = 5, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Adducts and medially rotates humerus", Name = "Chest" },
                        new { Id = 6, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Extends, adducts, and medially rotates humerus", Name = "Back" },
                        new { Id = 7, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Flexes and medially rotates arm", Name = "Shoulders" },
                        new { Id = 8, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Chief extensor of forearm", Name = "Triceps" },
                        new { Id = 9, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Supinates forearm and, when it is supine, flexes forearm", Name = "Biceps" },
                        new { Id = 10, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Flexes forearm", Name = "Forearms" },
                        new { Id = 11, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Elevates, retracts and rotates scapula", Name = "Trapezius" },
                        new { Id = 12, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "Flex the torso and thigh with respect to each other", Name = "Abs" }
                    );
                });

            modelBuilder.Entity("GymApp.Model.Program", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Description");

                    b.Property<bool>("IsPublic");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Programs");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Description = "A basic program", IsPublic = true, Name = "5x5", UserId = 1 }
                    );
                });

            modelBuilder.Entity("GymApp.Model.ProgramRating", b =>
                {
                    b.Property<int>("ProgramId");

                    b.Property<int>("UserId");

                    b.Property<DateTime>("GivenAt");

                    b.Property<int>("Rating");

                    b.HasKey("ProgramId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("ProgramRatings");

                    b.HasData(
                        new { ProgramId = 1, UserId = 1, GivenAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Rating = 5 }
                    );
                });

            modelBuilder.Entity("GymApp.Model.Session", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<int>("UserId");

                    b.Property<int?>("WorkoutId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.HasIndex("WorkoutId");

                    b.ToTable("Sessions");
                });

            modelBuilder.Entity("GymApp.Model.SessionBlock", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("ExerciseId");

                    b.Property<int>("SessionId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.HasIndex("SessionId");

                    b.ToTable("SessionBlocks");
                });

            modelBuilder.Entity("GymApp.Model.SessionSet", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("Reps");

                    b.Property<int>("Rest");

                    b.Property<int>("SessionBlockId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<decimal>("Weight")
                        .HasColumnType("decimal(5, 2)");

                    b.HasKey("Id");

                    b.HasIndex("SessionBlockId");

                    b.ToTable("SessionSets");
                });

            modelBuilder.Entity("GymApp.Model.Set", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BlockId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("Reps");

                    b.Property<int>("Rest");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<decimal>("Weight")
                        .HasColumnType("decimal(5, 2)");

                    b.HasKey("Id");

                    b.HasIndex("BlockId");

                    b.ToTable("Sets");

                    b.HasData(
                        new { Id = 1, BlockId = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 2, BlockId = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 3, BlockId = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 4, BlockId = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 5, BlockId = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 6, BlockId = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 7, BlockId = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 8, BlockId = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 9, BlockId = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 10, BlockId = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 11, BlockId = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 12, BlockId = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 13, BlockId = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 14, BlockId = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 15, BlockId = 3, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 16, BlockId = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 17, BlockId = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 18, BlockId = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 19, BlockId = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m },
                        new { Id = 20, BlockId = 4, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Reps = 5, Rest = 1, Weight = 0m }
                    );
                });

            modelBuilder.Entity("GymApp.Model.Settings", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("Units");

                    b.Property<DateTime?>("UpdatedAt");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId")
                        .IsUnique();

                    b.ToTable("Settings");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Units = 0, UserId = 1 }
                    );
                });

            modelBuilder.Entity("GymApp.Model.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("Users");

                    b.HasData(
                        new { Id = 1, AccessFailedCount = 0, ConcurrencyStamp = "2ff99bfe-77f7-4a53-9a60-acb6204deb42", Email = "admin@gymapp.com", EmailConfirmed = false, LockoutEnabled = false, NormalizedEmail = "admin@gymapp.com", PasswordHash = "AQAAAAEAACcQAAAAEMfFSSmZ8f0ICKtM0HXdE5pYWIf/RiIHHTAn9A4WtOd5A9uXc1UDfGiTWA18Ck0eVA==", PhoneNumberConfirmed = false, SecurityStamp = "b1b9ceb6-f567-48e0-b47f-aef8a9a78bc1", TwoFactorEnabled = false, UserName = "admin" }
                    );
                });

            modelBuilder.Entity("GymApp.Model.Workout", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt");

                    b.Property<string>("Name");

                    b.Property<int>("ProgramId");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("Id");

                    b.HasIndex("ProgramId");

                    b.ToTable("Workouts");

                    b.HasData(
                        new { Id = 1, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Name = "Workout A", ProgramId = 1 },
                        new { Id = 2, CreatedAt = new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), Name = "Workout B", ProgramId = 1 }
                    );
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("RoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<int>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("UserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<int>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.Property<int>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("UserTokens");
                });

            modelBuilder.Entity("GymApp.Model.Block", b =>
                {
                    b.HasOne("GymApp.Model.Exercise", "Exercise")
                        .WithMany()
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GymApp.Model.Workout", "Workout")
                        .WithMany("Blocks")
                        .HasForeignKey("WorkoutId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.Exercise", b =>
                {
                    b.HasOne("GymApp.Model.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("GymApp.Model.ExerciseMuscle", b =>
                {
                    b.HasOne("GymApp.Model.Exercise", "Exercise")
                        .WithMany("Muscles")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GymApp.Model.Muscle", "Muscle")
                        .WithMany("Exercises")
                        .HasForeignKey("MuscleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.Program", b =>
                {
                    b.HasOne("GymApp.Model.User", "User")
                        .WithMany("Programs")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.ProgramRating", b =>
                {
                    b.HasOne("GymApp.Model.Program", "Program")
                        .WithMany("Ratings")
                        .HasForeignKey("ProgramId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("GymApp.Model.User", "User")
                        .WithMany("Ratings")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("GymApp.Model.Session", b =>
                {
                    b.HasOne("GymApp.Model.User", "User")
                        .WithMany("Sessions")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GymApp.Model.Workout", "Workout")
                        .WithMany()
                        .HasForeignKey("WorkoutId");
                });

            modelBuilder.Entity("GymApp.Model.SessionBlock", b =>
                {
                    b.HasOne("GymApp.Model.Exercise", "Exercise")
                        .WithMany()
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GymApp.Model.Session", "Session")
                        .WithMany("SessionBlocks")
                        .HasForeignKey("SessionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.SessionSet", b =>
                {
                    b.HasOne("GymApp.Model.SessionBlock", "SessionBlock")
                        .WithMany("SessionSets")
                        .HasForeignKey("SessionBlockId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.Set", b =>
                {
                    b.HasOne("GymApp.Model.Block", "Block")
                        .WithMany("Sets")
                        .HasForeignKey("BlockId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.Settings", b =>
                {
                    b.HasOne("GymApp.Model.User", "User")
                        .WithOne("Settings")
                        .HasForeignKey("GymApp.Model.Settings", "UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("GymApp.Model.Workout", b =>
                {
                    b.HasOne("GymApp.Model.Program", "Program")
                        .WithMany("Workouts")
                        .HasForeignKey("ProgramId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<int>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole<int>")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<int>", b =>
                {
                    b.HasOne("GymApp.Model.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<int>", b =>
                {
                    b.HasOne("GymApp.Model.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<int>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole<int>")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("GymApp.Model.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<int>", b =>
                {
                    b.HasOne("GymApp.Model.User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
